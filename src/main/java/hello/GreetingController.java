package hello;

import hello.entity.Employee;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@Controller
public class GreetingController {

    @GetMapping("/allEmployees")
    public String greeting(@RequestParam(name="id", required=false, defaultValue="1") String id, Model model) {
        String uri = "http://localhost:8000/rest/emp/";
        RestTemplate restTemplate = new RestTemplate();
        Employee[] employees = restTemplate.getForObject(uri, Employee[].class);

        //model.addAttribute("name", e.getName());
        model.addAttribute("employees", employees);
        model.addAttribute("employee", new Employee());
//        System.out.println(employees);
        return "home";
    }

    @PostMapping("/postEmployee")
    public String postEmployee(@ModelAttribute("employee") Employee employee, BindingResult error, Model model){

//        System.out.println(employee.getId() + employee.getName());
//        System.out.println();
        String uri = "http://localhost:8000/rest/emp";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(uri, employee, Employee.class);
        return "success";
    }
}
